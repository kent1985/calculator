﻿namespace Calculator.Constants
{
    /// <summary>
    /// Contains indication about operation's priority.
    /// </summary>
    internal class OperationPriority
    {
        /// <summary>
        /// Indicates low priority.
        /// </summary>
        internal const int LowerPriority = 0;
        /// <summary>
        /// Indicates higher priority.
        /// </summary>
        internal const int HigherPriority = 1;
    }
}
