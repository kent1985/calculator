﻿namespace Calculator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Calculator.Constants;

    /// <summary>
    /// <see cref="Operation"/> class that performs calculator operation.
    /// </summary>
    public class Operation
    {
        private const string OpenBracket = "(";
        private const string ClosingBracket = ")";
        private static readonly List<string> possibleOperations = new List<string> { "+", "-", "*", "/" };

        /// <summary>
        /// Perform calculator operation based on provided value in <paramref name="sum"/>.
        /// Assumption that the <paramref name="sum"/> parameter will always consist of numbers and operators separated by spaces.
        /// </summary>
        /// <param name="sum">String that contains operation to be performed. Consists of numbers and parameters that separated by spaces.</param>
        /// <returns>Result of the calculator operation.</returns>
        public static decimal Calculate(string sum)
        {
            if (sum.Contains(OpenBracket) || sum.Contains(ClosingBracket))
            {
                return PerformOperationWithBracket(sum);
            }
            else
            {
                return ProcessOperationWithoutBracket(sum);
            }
        }

        /// <summary>
        /// Perform mathematical operation with brackets.
        /// </summary>
        /// <param name="operationExpression">Mathematical expression with numbers and operators are separated by spaces.</param>
        /// <returns>Result of the mathematical operation.</returns>
        private static decimal PerformOperationWithBracket(string operationExpression)
        {
            // Declare a list of expression to store possible operations.
            List<string> expressions = new List<string>();

            // Declare a pipe (|) indicator.
            string pipeIndicator = "|";

            do
            {
                // Checks for pair of brackets ()
                int startIndex = operationExpression.LastIndexOf(OpenBracket);
                int endIndex = operationExpression.IndexOf(ClosingBracket);

                // Additional check to ensure endIndex of the closing bracket always greater than opening bracket.
                while (endIndex < startIndex && endIndex != operationExpression.Length - 1)
                {
                    endIndex = operationExpression.IndexOf(ClosingBracket, endIndex + 1);
                }

                int length = (endIndex - startIndex) + 1;
                string subString = operationExpression.Substring(startIndex, length);
                expressions.Add(subString.Replace(OpenBracket, string.Empty).Replace(ClosingBracket, string.Empty).Trim());

                // Use pipe (|) to indicate it should be replace by the result of operation in the bracket
                operationExpression = operationExpression.Remove(startIndex, length).Insert(startIndex, pipeIndicator).Trim();
            }
            while (operationExpression.Contains(OpenBracket) || operationExpression.Contains(ClosingBracket));

            // Add final processed expression.
            expressions.Add(operationExpression);

            // Initialize result value.
            decimal finalResult = 0;
            List<decimal> results = new List<decimal>();
            foreach (string expression in expressions)
            {
                if (!expression.Contains(pipeIndicator))
                {
                    if (possibleOperations.Any(x => expression.Contains(x)))
                    {
                        // Perform operation without bracket.
                        finalResult = ProcessOperationWithoutBracket(expression);
                    }
                    else
                    {
                        // Does not contains pipe indicator, it is a number within a bracket after filter.
                        finalResult = Convert.ToDecimal(expression);
                    }

                    // Stores the result to be used in later operation, if any.
                    results.Insert(0, finalResult);
                }
                else
                {
                    // Checks for indicator occurence.
                    int occurence = expression.Count(x => x == '|');
                    if (occurence == 1)
                    {
                        // Only occurs once, will use previous result operation as value for operation.
                        string newOperation = expression.Replace(pipeIndicator, results.First().ToString());
                        results.RemoveAt(0);
                        finalResult = ProcessOperationWithoutBracket(newOperation);

                        // Replace result after operation.
                        results.Insert(0, finalResult);
                    }
                    else
                    {
                        //  Indicates whether there is first left side operation has been performed.
                        bool hasLeftResult = false;
                        string operation = expression;

                        // Replace each indicator with result in ascending order.
                        foreach (decimal result in results)
                        {
                            // Replace pipe indicator with value in result.
                            if (operation.Contains(pipeIndicator))
                            {
                                int indexOfPipe = operation.IndexOf(pipeIndicator);
                                operation = operation.Remove(indexOfPipe, 1).Insert(indexOfPipe, result.ToString());
                            }
                        }

                        // Split string to perform operation.
                        string[] splitOperations = operation.Split(' ');
                        for (int i = 0; i < splitOperations.Length; i++)
                        {
                            // Use modulus 2 not equal zero as consider operator always in odd index of an array. Bracket already been removed.
                            if (i % 2 != 0)
                            {
                                if (hasLeftResult)
                                {
                                    // Use previous result as left side number to perform operation.
                                    finalResult = PerformOperation(finalResult, Convert.ToDecimal(splitOperations[i + 1]), splitOperations[i]);
                                }
                                else
                                {
                                    // Use left side number in array to perform operation.
                                    finalResult = PerformOperation(Convert.ToDecimal(splitOperations[i - 1]), Convert.ToDecimal(splitOperations[i + 1]), splitOperations[i]);
                                    hasLeftResult = true;
                                }
                            }
                        }
                    }
                }
            }

            return finalResult;
        }

        /// <summary>
        /// Process normal mathematical operation without bracket.
        /// </summary>
        /// <param name="expression">Mathematical expression with numbers and operators are separated by spaces.</param>
        /// <returns>Result of the mathematical operation.</returns>
        private static decimal ProcessOperationWithoutBracket(string expression)
        {
            // Splits expression into array.
            string[] splitExpressions = expression.Split(" ".ToCharArray());

            int previousOperatorPriority = -1;
            int previousIndex = -1;
            int nextOperatorPriority = -1;

            // Indicates final high priority (*,/) to check as final right side operation.
            int finalHighPriorityIndex = 0;

            // Declare key value pair to store index of array with operator string.
            List<KeyValuePair<int, string>> operations = new List<KeyValuePair<int, string>>();

            // Purpose of this loop is to prioritize order of operation in the splitExpressions.
            for (int i = 0; i < splitExpressions.Length; i++)
            {
                // Use modulus 2 not equal zero as consider operator always in odd index of an array. No brackets involved.
                if (i % 2 != 0)
                {
                    // Check for first time operator priority.
                    if (previousOperatorPriority == -1)
                    {
                        previousOperatorPriority = GetPriority(splitExpressions[i]);
                        operations.Add(new KeyValuePair<int, string>(i, splitExpressions[i]));
                        previousIndex = 0;
                    }
                    else
                    {
                        nextOperatorPriority = GetPriority(splitExpressions[i]);
                    }

                    // Starts perform operation.
                    if (previousOperatorPriority != -1 && nextOperatorPriority != -1)
                    {
                        // Checks if next operator's priority is higher, i.e. *,/ greater than +,-
                        if (nextOperatorPriority > previousOperatorPriority)
                        {
                            if (operations.Any())
                            {
                                operations.Insert(previousIndex, new KeyValuePair<int, string>(i, splitExpressions[i]));
                                finalHighPriorityIndex = previousIndex;
                            }
                            else
                            {
                                operations.Add(new KeyValuePair<int, string>(i, splitExpressions[i]));
                                previousIndex = 0;
                                finalHighPriorityIndex = previousIndex;
                            }
                        }
                        // Checks if the priority is the same.
                        else if (nextOperatorPriority == previousOperatorPriority)
                        {
                            if (operations.Any())
                            {
                                // Checks if it belongs to higher priority (*,/)
                                if (nextOperatorPriority == OperationPriority.HigherPriority)
                                {
                                    previousIndex += 1;
                                    operations.Insert(previousIndex, new KeyValuePair<int, string>(i, splitExpressions[i]));
                                    // Update final high priority index.
                                    finalHighPriorityIndex = previousIndex;
                                }
                                else
                                {
                                    operations.Add(new KeyValuePair<int, string>(i, splitExpressions[i]));
                                }
                            }
                            else
                            {
                                operations.Add(new KeyValuePair<int, string>(i, splitExpressions[i]));
                                previousIndex = 0;
                            }
                        }
                        else
                        {
                            operations.Add(new KeyValuePair<int, string>(i, splitExpressions[i]));
                            // Update previous index for the next higher priority operation to be added, if any.
                            previousIndex += 1;
                        }

                        previousOperatorPriority = nextOperatorPriority;
                    }
                }
            }

            decimal leftSideResult = 0;
            bool hasRightSideResult = false;

            // Temporary store right side operation.
            List<decimal> tempRightSideResult = new List<decimal>();
            bool isFinalHighPriorityReached = false;

            // Loops the list of operation.
            for (int i = 0; i < operations.Count; i++)
            {
                if (i == 0)
                {
                    leftSideResult = PerformOperation(Convert.ToDecimal(splitExpressions[operations[i].Key - 1]), Convert.ToDecimal(splitExpressions[operations[i].Key + 1]), operations[i].Value);
                }
                else
                {
                    // Checks if the index of the operation is greater than previous.
                    if (operations[i].Key > operations[i - 1].Key)
                    {
                        if (IsNextSequence(operations[i].Key, operations[i - 1].Key) && !IsHighPriority(operations[i].Key, splitExpressions))
                        {
                            leftSideResult = PerformOperation(leftSideResult, Convert.ToDecimal(splitExpressions[operations[i].Key + 1]), operations[i].Value);
                        }
                        else
                        {
                            decimal rightSideResult;
                            if (hasRightSideResult)
                            {
                                if (!isFinalHighPriorityReached)
                                {
                                    rightSideResult = PerformOperation(Convert.ToDecimal(splitExpressions[operations[i].Key - 1]), Convert.ToDecimal(splitExpressions[operations[i].Key + 1]), operations[i].Value);
                                    // Add result of higher priority on the right side of the operation in the string into temporary list.
                                    tempRightSideResult.Add(rightSideResult);
                                }
                                else
                                {
                                    if (!IsHighPriority(operations[i].Key, splitExpressions))
                                    {
                                        leftSideResult = PerformOperation(leftSideResult, Convert.ToDecimal(splitExpressions[operations[i].Key + 1]), operations[i].Value);
                                    }
                                    else
                                    {
                                        leftSideResult = PerformOperation(leftSideResult, tempRightSideResult.First(), operations[i].Value);
                                        // Remove result once operation has been performed.
                                        if (tempRightSideResult.Count > 0)
                                        {
                                            tempRightSideResult.RemoveAt(0);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // First time right side operation.
                                rightSideResult = PerformOperation(Convert.ToDecimal(splitExpressions[operations[i].Key - 1]), Convert.ToDecimal(splitExpressions[operations[i].Key + 1]), operations[i].Value);
                                tempRightSideResult.Add(rightSideResult);
                                hasRightSideResult = true;
                            }
                        }
                    }
                    else
                    {
                        leftSideResult = PerformOperation(Convert.ToDecimal(splitExpressions[operations[i].Key - 1]), leftSideResult, operations[i].Value);
                    }

                    if (i == finalHighPriorityIndex)
                    {
                        isFinalHighPriorityReached = true;
                    }
                }
            }

            return leftSideResult;
        }

        /// <summary>
        /// Checks if current operator is next in sequence to the previous operator.
        /// </summary>
        /// <param name="currentIndex">Index of current operator.</param>
        /// <param name="previousIndex">Previous index of previous operator to check.</param>
        /// <returns><see lang="true"/> if it is next in sequence, otherwise <see lang="false"/>.</returns>
        private static bool IsNextSequence(int currentIndex, int previousIndex)
        {
            int previousOperatorIndexLength = 2;
            return currentIndex - previousOperatorIndexLength == previousIndex;
        }

        /// <summary>
        /// Checks if next operator belongs to <see cref="OperationPriority.HigherPriority"/> group.
        /// </summary>
        /// <param name="index">Current operator's index.</param>
        /// <param name="splitExpression">The split expression of the provided string operation.</param>
        /// <returns><see lang="true"/> if it is <see cref="OperationPriority.HigherPriority"/>, otherwise <see lang="false"/>.</returns>
        private static bool IsHighPriority(int index, string[] splitExpression)
        {
            int nextOperatorIndexLength = 2;
            if (splitExpression.Length > (index + nextOperatorIndexLength))
            {
                return GetPriority(splitExpression[index + nextOperatorIndexLength]) == OperationPriority.HigherPriority;
            }

            return false;
        }

        /// <summary>
        /// Performs mathematical operation based on operator.
        /// </summary>
        /// <param name="leftNumber">Left side number of the operation.</param>
        /// <param name="rightNumber">Right side number of the operation.</param>
        /// <param name="operatorExpression">Operator expression.</param>
        /// <returns>Result of the mathematical operation.</returns>
        /// <exception cref="NotSupportedException">Thrown if <paramref name="operatorExpression"/> is NOT addition (+), subtract (-), multiplication (*) or division (/).</exception>
        private static decimal PerformOperation(decimal leftNumber, decimal rightNumber, string operatorExpression)
        {
            switch (operatorExpression)
            {
                case "+":
                    return leftNumber + rightNumber;
                case "-":
                    return leftNumber - rightNumber;
                case "*":
                    return leftNumber * rightNumber;
                case "/":
                    return leftNumber / rightNumber;
                default:
                    throw new NotSupportedException($"PerformOperation | String operator provided is not supported. Operator {operatorExpression}.");
            }
        }

        /// <summary>
        /// Gets the priority of the operation based on provided operator expression.
        /// </summary>
        /// <param name="operatorExpression">Possible operator symbol includes addition (+), subtract (-), multiplication (*), division (/).</param>
        /// <returns><see cref="OperationPriority.HigherPriority"/> if multiplication or division, <see cref="OperationPriority.LowerPriority"/> if addition or subtraction.</returns>
        /// <exception cref="NotSupportedException">Thrown if <paramref name="operatorExpression"/> is NOT addition (+), subtract (-), multiplication (*) or division (/).</exception>
        private static int GetPriority(string operatorExpression)
        {
            switch (operatorExpression)
            {
                case "+":
                case "-":
                    return OperationPriority.LowerPriority;
                case "*":
                case "/":
                    return OperationPriority.HigherPriority;
                default:
                    throw new NotSupportedException($"GetPriority | String operator provided is not supported. Operator { operatorExpression }.");
            }
        }
    }
}
