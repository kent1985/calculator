﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalculatorTests
{
    [TestClass]
    public class OperationTests
    {
        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        [DeploymentItem("\\TestFile\\TestCalculator.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML",
                   "|DataDirectory|\\TestFile\\TestCalculator.xml",
                   "Row",
                    DataAccessMethod.Sequential)]
        [TestMethod]
        public void Calculate_GivenStringExpression_ShouldReturnExpectedResult()
        {
            string expression = (string)TestContext.DataRow["Expression"];
            decimal expectedResult = Decimal.Parse((string)TestContext.DataRow["Result"]);
            Assert.AreEqual(expectedResult, Calculator.Operation.Calculate(expression));
        }
    }
}
